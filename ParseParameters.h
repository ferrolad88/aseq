#ifndef PARSEPARAMETERS_H_INCLUDED
#define PARSEPARAMETERS_H_INCLUDED

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "DataStructures.h"
#include "Utilities.h"

int MAX_READ_BUFF=1000000;

/////////////////////////////////////////////////////////////////////////////////////
// Parse arguments
/////////////////////////////////////////////////////////////////////////////////////

struct input_args *getInputArgs(char *argv[],int argc)
{
    int i;
    struct input_args *arguments = (struct input_args *)malloc(sizeof(struct input_args));
    arguments->cores = 1;
    arguments->mbq = 1;
    arguments->mrq = 1;
    arguments->gene_stat = 1;
    arguments->snp_stat = 0.5;
    arguments->mode = (char*)malloc(sizeof(char)*7);
    sprintf(arguments->mode,"PILEUP");
    arguments->vcf = NULL;
    arguments->bed = NULL;
    arguments->genes = NULL;
    arguments->transcripts = NULL;
    arguments->bam = NULL;
    arguments->bamlist = NULL;
    arguments->vcflist = NULL;
    arguments->bamfiles = NULL;
    arguments->HT_pvalue = arguments->FT_pvalue = 0.05;
    arguments->HT_perc = 0.2;
    arguments->HT_zeros = 1;
    arguments->HT_binom = 1;
    arguments->mrn = 1;
    arguments->granularity = 1;
    arguments->outdir=NULL;
    arguments->gt_mode = 0;
    arguments->pileup_blocks = 1000;
    arguments->pileup_mode = 0;
    arguments->pileup_light_out = 0;
    char *tmp=NULL;

    for(i=1;i<argc;i++)
    {
        if( strncmp(argv[i],"mode=",5) == 0 )
        {
           free(arguments->mode);
           arguments->mode = (char*)malloc(sizeof(char)*strlen(argv[i])+1);
           strcpy(arguments->mode,argv[i]+5);
        }
        else if( strncmp(argv[i],"bed=",4) == 0 )
        {
            arguments->bed = (char*)malloc(sizeof(char)*strlen(argv[i])+1);
            strcpy(arguments->bed,argv[i]+4);
			subSlash(arguments->bed);
        }
        else if( strncmp(argv[i],"genes=",6) == 0 )
        {
            arguments->genes = (char*)malloc(sizeof(char)*strlen(argv[i])+1);
            strcpy(arguments->genes,argv[i]+6);
			subSlash(arguments->genes);
        }
        else if( strncmp(argv[i],"transcripts=",12) == 0 )
        {
            arguments->transcripts = (char*)malloc(sizeof(char)*strlen(argv[i])+1);
            strcpy(arguments->transcripts,argv[i]+12);
			subSlash(arguments->transcripts);
	    arguments->gt_mode=1;
        }
        else if( strncmp(argv[i],"vcf=",4) == 0 )
        {
            arguments->vcf= (char*)malloc(sizeof(char)*strlen(argv[i])+1);
            strcpy(arguments->vcf,argv[i]+4);
			subSlash(arguments->vcf);
        }
        else if( strncmp(argv[i],"bam=",4) == 0 )
        {
            arguments->bam = (char*)malloc(sizeof(char)*strlen(argv[i])+1);
            strcpy(arguments->bam,argv[i]+4);
			subSlash(arguments->bam);
        }
        else if( strncmp(argv[i],"bamlist=",8) == 0 )
        {
            arguments->bamlist = (char*)malloc(sizeof(char)*strlen(argv[i])+1);
            strcpy(arguments->bamlist,argv[i]+8);
			subSlash(arguments->bamlist);
        }
        else if( strncmp(argv[i],"vcflist=",8) == 0 )
        {
            arguments->vcflist = (char*)malloc(sizeof(char)*strlen(argv[i])+1);
            strcpy(arguments->vcflist,argv[i]+8);
			subSlash(arguments->vcflist);
        }
        else if( strncmp(argv[i],"threads=",8) == 0 )
        {
            tmp = (char*)malloc(strlen(argv[i])-7);
            strcpy(tmp,argv[i]+8);
            arguments->cores = atoi(tmp);
            free(tmp);
        }
        else if( strncmp(argv[i],"mbq=",4) == 0 )
        {
            tmp = (char*)malloc(strlen(argv[i])-3);
            strcpy(tmp,argv[i]+4);
            arguments->mbq = atoi(tmp);
            free(tmp);
        }
        else if( strncmp(argv[i],"mrq=",4) == 0 )
        {
            tmp = (char*)malloc(strlen(argv[i])-3);
            strcpy(tmp,argv[i]+4);
            arguments->mrq = atoi(tmp);
            free(tmp);
        }
        else if( strncmp(argv[i],"mdc=",4) == 0 )
        {
            tmp = (char*)malloc(strlen(argv[i])-3);
            strcpy(tmp,argv[i]+4);
            arguments->mrn = atoi(tmp);
            free(tmp);
        }
        else if( strncmp(argv[i],"pht=",4) == 0 )
        {
            tmp = (char*)malloc(strlen(argv[i])-3);
            strcpy(tmp,argv[i]+4);
            arguments->HT_pvalue = atof(tmp);
            free(tmp);
        }
        else if( strncmp(argv[i],"zht",3) == 0 )
        {
            arguments->HT_zeros = 0;
        }
        else if( strncmp(argv[i],"pildyn",6) == 0 )
        {
            arguments->pileup_mode=1;
        }
	   else if( strncmp(argv[i],"plight",6) == 0 )
        {
            arguments->pileup_light_out=1;
        }
        else if(strncmp(argv[i],"pilblock=",9) == 0 )
        {
            tmp = (char*)malloc(strlen(argv[i])-8);
            strcpy(tmp,argv[i]+9);
            arguments->pileup_blocks = atoi(tmp);
            free(tmp);
        }
        else if( strncmp(argv[i],"htperc=",7) == 0 )
        {
            tmp = (char*)malloc(strlen(argv[i])-6);
            strcpy(tmp,argv[i]+7);
            arguments->HT_perc = atof(tmp);
            arguments->HT_binom = 0;
            free(tmp);
        }
        else if( strncmp(argv[i],"pft=",4) == 0 )
        {
            tmp = (char*)malloc(strlen(argv[i])-3);
            strcpy(tmp,argv[i]+4);
            arguments->FT_pvalue = atof(tmp);
            free(tmp);
        }
        else if( strncmp(argv[i],"out=",4) == 0 )
        {
            arguments->outdir = (char*)malloc(strlen(argv[i])-3);
            strcpy(arguments->outdir,argv[i]+4);
			subSlash(arguments->outdir);
        }
        else if( strncmp(argv[i],"genestat=",9) == 0 )
        {
            tmp = (char*)malloc(strlen(argv[i])-8);
            strcpy(tmp,argv[i]+9);
            arguments->gene_stat = atoi(tmp);
            free(tmp);
        }
        else if( strncmp(argv[i],"snpstat=",8) == 0 )
        {
            tmp = (char*)malloc(strlen(argv[i])-7);
            strcpy(tmp,argv[i]+8);
            arguments->gene_stat = atof(tmp);
            free(tmp);
        } else if( strncmp(argv[i],"granularity=",12) == 0 )
        {
            tmp = (char*)malloc(strlen(argv[i])-11);
            strcpy(tmp,argv[i]+12);
            arguments->granularity = atoi(tmp);
            free(tmp);
        }
        else
        {
            fprintf(stderr,"ERROR: input parameters not valid.\n");
            exit(1);
        }
    }
    return arguments;
}


int checkInputArgs(struct input_args *arguments)
{
    if( strncmp(arguments->mode,"ASE",3) != 0 && strncmp(arguments->mode,"PILEUP",6) != 0 && strncmp(arguments->mode,"GENOTYPE",8) != 0 )
    {
        fprintf(stderr,"ERROR: mode not correct.\n");
        return 1;
    }

    if( strncmp(arguments->mode,"PILEUP",6) == 0 )
    {
        if ( (arguments->vcf == NULL && arguments->bed == NULL && arguments->vcflist == NULL && arguments->genes == NULL) | (arguments->bam == NULL && arguments->bamlist == NULL))
        {
             fprintf(stderr,"ERROR: vcf, genes or bam files not present.\n");
             return 1;
        }
    } else if( strncmp(arguments->mode,"ASE",3) == 0 )
    {
        if ( arguments->vcflist == NULL | (arguments->genes == NULL & arguments->transcripts == NULL) | arguments->bamlist == NULL)
        {
             fprintf(stderr,"ERROR: genes, vcf or bam files lists not present.\n");
             return 1 ;
        }

        if(arguments->vcf != NULL)
        {
            free(arguments->vcf);
            arguments->vcf = NULL;
        }

    } else if( strncmp(arguments->mode,"GENOTYPE",8) == 0 )
    {
        if (arguments->vcf == NULL | (arguments->bam == NULL && arguments->bamlist == NULL))
        {
             fprintf(stderr,"ERROR: vcf or bam files not present.\n");
             return 1 ;
        }
    }

    if (arguments->cores <=0)
    {
        fprintf(stderr,"ERROR: the number of threads is not valid.\n");
        return 1 ;
    }

    if (arguments->mbq <0)
    {
        fprintf(stderr,"ERROR: the min base squality is not valid.\n");
        return 1 ;
    }

    if (arguments->mrq <0)
    {
        fprintf(stderr,"ERROR: the mkin read quality is not valid.\n");
        return 1 ;
    }
    
    if (arguments->genes != NULL & arguments->transcripts != NULL)
    {
    	fprintf(stderr,"ERROR: Specify only genes or transcripts file.\n");
        return 1 ;
    }

    if (arguments->bam != NULL)
    {
        arguments->bamfiles = malloc(sizeof(char*));
        arguments->bamindexes = malloc(sizeof(bam_index_t *));
        arguments->bamfiles[0] = arguments->bam;

        if (FileExists(arguments->bamfiles[0])==1)
        {
            fprintf(stderr,"WARNING: BAM file %s not present. \n",arguments->bamfiles[0]);
            return 1;
        }

        arguments->bamindexes[0] = bam_index_load(arguments->bamfiles[0]);
        if (arguments->bamindexes[0] == 0)
        {
                arguments->bamindexes[0] = bam_index_build(arguments->bamfiles[0]);
                fprintf(stderr,"WARNING: bam indexing file not present. Will be created now.\n");
                arguments->bamindexes[0] = bam_index_load(arguments->bamfiles[0]);
        	if (arguments->bamindexes[0] == 0)
        	{
        		fprintf(stderr,"ERROR: unable to create index for BAM file.\n",arguments->bamfiles[0]);
        		return 1;
        	}
        }
        arguments->bamfiles_number = 1;

        if (arguments->bamlist != NULL)
        {
            fprintf(stderr,"WARNING: BAM files list will be ignored.\n");
        }
    }

    if (arguments->bam == NULL && arguments->bamlist != NULL)
    {
        FILE *file = fopen(arguments->bamlist,"r");

        if  (file==NULL)
        {
            fprintf(stderr,"ERROR: BAM files list not present.\n");
            return 1;
        }

        arguments->bamfiles_number = 0;

        char line [MAX_READ_BUFF];
        while(fgets(line,sizeof(line),file) != NULL )
        {
            arguments->bamfiles_number++;
        }

        rewind(file);
        arguments->bamfiles = malloc(sizeof(char*)*arguments->bamfiles_number);
        arguments->bamindexes = malloc(sizeof(bam_index_t *)*arguments->bamfiles_number);
        int counter=0;
        int size=0;

        while(fgets(line,sizeof(line),file) != NULL )
        {
            if (line[strlen(line)-1]=='\n')
                size = strlen(line);
            else
                size = strlen(line)+1;
            arguments->bamfiles[counter] = (char*)malloc(size);
            strncpy(arguments->bamfiles[counter],line,size-1);
            (arguments->bamfiles[counter])[size-1] = '\0';
			subSlash(arguments->bamfiles[counter]);

            if (FileExists(arguments->bamfiles[counter])==1)
            {
                fprintf(stderr,"ERROR: BAM file %s not present. \n",arguments->bamfiles[counter]);
                return 1;
            }

            // load BAM index
            arguments->bamindexes[counter] = bam_index_load(arguments->bamfiles[counter]);
            if (arguments->bamindexes[counter] == 0)
            {
                    arguments->bamindexes[counter] = bam_index_build(arguments->bamfiles[counter]);
                    fprintf(stderr,"WARNING: BAM indexing of file %s not present. Will be created now.\n",arguments->bamfiles[counter]);
                    arguments->bamindexes[counter] = bam_index_load(arguments->bamfiles[counter]);
            	    if (arguments->bamindexes[counter] != 0)
            	    {
            	    	counter++;
            	    }
            	    else
            	    {
            	    	 fprintf(stderr,"WARNING: unable to create BAM index for file %s.\n",arguments->bamfiles[counter]);
            	    }
            }
            else
            {
            	counter++;
            }
        }
        
        if (counter==0)
        {
        	 fprintf(stderr,"ERROR: index creation failed for all BAM files.\n",arguments->bamfiles[counter]);
        	 return 1;
        }
        
    }

    if (arguments->vcf != NULL)
    {
        //arguments->vcffiles = malloc(sizeof(char*));
        //arguments->vcffiles[0] = arguments->vcf;

        //if (FileExists(arguments->vcffiles[0])==1)
        //{
        //   fprintf(stderr,"WARNING: vcf file %s not present. \n",arguments->vcffiles[0]);
        //    return 1;
        //}

        arguments->vcffiles_number = 1;

        if (arguments->vcflist != NULL)
        {
            fprintf(stderr,"WARNING: vcf files list will be ignored.\n");
        }
        if (arguments->bed != NULL)
        {
            fprintf(stderr,"WARNING: bed file list will be ignored.\n");
        }
    }
    
    if (arguments->vcf == NULL && arguments->bed != NULL)
    {
    	arguments->vcffiles_number = 1;

        if (arguments->vcflist != NULL)
        {
            fprintf(stderr,"WARNING: vcf files list will be ignored.\n");
        }
    }

    if (arguments->vcf == NULL && arguments->vcflist != NULL && arguments->bed == NULL)
    {
        FILE *file = fopen(arguments->vcflist,"r");

        if  (file==NULL)
        {
            fprintf(stderr,"ERROR: vcf files list %s not present.\n",arguments->vcflist);
            return 1;
        }

        arguments->vcffiles_number = 0;

        char line [MAX_READ_BUFF];
        while(fgets(line,sizeof(line),file) != NULL )
        {
            arguments->vcffiles_number++;
        }

        rewind(file);
        arguments->vcffiles = malloc(sizeof(char*)*arguments->vcffiles_number);
        int counter=0;
        int size = 0;

        while(fgets(line,sizeof(line),file) != NULL )
        {
            if (line[strlen(line)-1]=='\n')
                size = strlen(line);
            else
                size = strlen(line)+1;
            arguments->vcffiles[counter] = (char*)malloc(size);
            strncpy(arguments->vcffiles[counter],line,size-1);
            (arguments->vcffiles[counter])[size-1] = '\0';
			subSlash(arguments->vcffiles[counter]);

			if (FileExists(arguments->vcffiles[counter])==1)
            {
                fprintf(stderr,"WARNING: vcf file %s not present. \n",arguments->vcffiles[counter]);
                return 1;
            }

            counter++;
        }
    }

    if (arguments->bamlist != NULL && arguments->vcflist != NULL && arguments->vcffiles_number != arguments->bamfiles_number)
    {
        fprintf(stderr,"ERROR: lengths of vcf and bam files lists are not matching.\n");
        return 1;
    }

    if (arguments->snp_stat<0 || arguments->snp_stat>1)
    {
        fprintf(stderr,"ERROR: snpstat option not valid. Value should be in [0,1].\n");
        return 1 ;
    }

    if (arguments->gene_stat<0 || arguments->gene_stat>arguments->bamfiles_number)
    {
        fprintf(stderr,"ERROR: genestat option not valid. Value should be in [0,number fo samples].\n");
        return 1 ;
    }
    
    if ( (arguments->HT_pvalue<0 || arguments->HT_pvalue>1) || (arguments->FT_pvalue<0 || arguments->FT_pvalue>1) || (arguments->HT_perc<0 || arguments->HT_perc>1) )
    {
        fprintf(stderr,"ERROR: p-values and percentages should be in [0,1].\n");
        return 1 ;
    }
    
    return 0;
}


#endif // PARSEPARAMETERS_H_INCLUDED
